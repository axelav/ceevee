# ceevee

my cv

## routes

route             | file              | description                     |
------------------|-------------------|---------------------------------|
`/`               | `views/main.js`   | the main view
`/*`              | `views/404.js`    | display unhandled routes

## commands
command               | description                                      |
----------------------|--------------------------------------------------|
`$ npm start`         | start the development server
`$ npm test`          | lint, validate deps & run tests
`$ npm run build`     | compile all files into `dist/`
`$ npm run inspect`   | inspect the bundle's dependencies
