const html = require('choo/html')

const TITLE = 'NOT FOUND'

module.exports = view

function view (state, emit) {
  if (state.title !== TITLE) {
    emit(state.events.DOMTITLECHANGE, TITLE)
  }

  return html`
    <body class="sans-serif f5 mw7 center mv6 ph2 ph0-l">
      <h1 class="f1 lh-solid tracked ttu mb4">
        not found
      </h1>
      <a href="/" class="link dark-blue underline-hover">
        Back to main
      </a>
    </body>
  `
}
