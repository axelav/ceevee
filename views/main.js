const html = require('choo/html')
const css = require('sheetify')
const { format } = require('fecha')
const sortBy = require('sort-by')

const TITLE = 'AXEL ANDERSON'

module.exports = view

function renderDate(date) {
  if (!date) {
    return 'today'
  }

  return format(new Date(date), 'MMMM YYYY')
}

const styles = css`
  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    margin-top: 0;
  }
`

const compareStrings = (x, y) => {
  const a = x.toLowerCase()
  const b = y.toLowerCase()

  if (a < b) {
    return -1
  }

  if (a > b) {
    return 1
  }

  return 0
}

function view({ title, events, data, deps }, emit) {
  if (title !== TITLE) {
    emit(events.DOMTITLECHANGE, TITLE)
  }

  const { contact, education, work, skills, interests } = data

  // TODO add bio
  // const BIO = `
  //   I'm a full-stack engineer that comes from a background in the arts & editorial. I've worked with artists & galleries, scrappy & established media companies, global brands & agencies, seed stage startups & Fortune 500 companies. I work best in JS and have some fluency in Java, Python & Go.

  //   I'm currently located in New York but am open to opportunities elsewhere as well as work with remote teams.
  // `

  return html`
    <body class="sans-serif f4 mw7 center mv6 ph2 ph0-l ${styles}">
      <section class="contact mb6">
        <div class="tr">
          <h1
            class="f1 lh-solid tracked-mega-l tracked ttu mb4"
            style="margin-right: -0.25em"
          >
            ${contact.name}
          </h1>
          <div>
            <a
              href="mailto:${contact.email}"
              class="link dark-blue underline-hover db di-ns mb1 mb0-ns"
            >
              ${contact.email}
            </a>
            <span class="dn di-ns">${' | '}</span>
            <a
              href="${contact.github}"
              class="link dark-blue underline-hover db di-ns"
              target="_blank"
            >
              ${contact.github.replace('https://', '')}
            </a>
            <span class="dn di-ns">${' | '}</span>
            <a
              href="${contact.website}"
              class="link dark-blue underline-hover db di-ns"
              target="_blank"
            >
              ${contact.website.replace('https://', '')}
            </a>
          </div>
        </div>
      </section>
      <section class="skills mb6">
        <h2 class="f2 lh-title mb4">
          Skills
        </h2>
        <ul class="lh-copy">
          ${skills.map((x) => html` <li>${x}</li> `)}
        </ul>
      </section>
      <section class="work mb6">
        <h2 class="f2 lh-title mb4">
          Work
        </h2>
        ${work.sort(sortBy('-start_date')).map((block) => {
          return html`
            <div class="mb5">
              <div class="flex-ns flex-row justify-between">
                <h3 class="f3 lh-title mb3">
                  ${block.position}
                </h3>
                <h4 class="f4 f3-ns lh-title mb3">
                  ${block.company.name}
                </h4>
              </div>
              <div class="flex-ns flex-row justify-between mb3">
                <div>
                  ${renderDate(block.start_date)} —
                  ${renderDate(block.end_date)}
                </div>
                <div class="dn db-ns">
                  ${block.company.location}
                </div>
              </div>
              <p class="lh-copy mt0 mb3 measure-wide">
                ${block.description}
              </p>
              ${block.technologies.length
                ? html` <h5 class="f5 lh-title mb3">Technologies</h5> `
                : null}
              <ul class="list cf pl0 f5 measure-wide">
                ${block.technologies.sort(compareStrings).map(
                  (x, i) => html`
                    <li class="fl pr1">
                      ${x}${i + 1 !== block.technologies.length ? ', ' : null}
                    </li>
                  `
                )}
              </ul>
            </div>
          `
        })}
      </section>
      <section class="education mb6">
        <h2 class="f2 lh-title mb4">
          Education
        </h2>
        <div class="flex-ns flex-row justify-between">
          <h3 class="f3 lh-title mb3">
            ${education.description}
          </h3>
          <h4 class="f4 f3-ns lh-title mb3">
            ${education.institution.name}
          </h4>
        </div>
        <div class="flex-ns flex-row justify-between mb3">
          <div>
            ${renderDate(education.start_date)} —
            ${renderDate(education.end_date)}
          </div>
          <div class="dn db-ns">
            ${education.institution.location}
          </div>
        </div>
      </section>
      <section class="interests mb6">
        <h2 class="f2 lh-title mb4">
          Interests
        </h2>
        <ul class="list cf pl0 lh-copy">
          ${interests
            .sort(
              sortBy('name', (key, value) =>
                key === 'name' ? value.toLowerCase() : value
              )
            )
            .map((x, i) => {
              if (x.url) {
                return html`
                  <li class="fl pr1">
                    <a
                      class="link dark-blue underline-hover"
                      href="${x.url}"
                      target="_blank"
                      >${x.name}</a
                    >${i + 1 !== interests.length ? ', ' : null}
                  </li>
                `
              }

              return html`
                <li class="fl pr1">
                  ${x.name}${i + 1 !== interests.length ? ', ' : null}
                </li>
              `
            })}
        </ul>
      </section>
      <section class="metadata mb6">
        <p class="f6 mt0 mb3">
          Built with
          ${deps.map((x, i) => {
            return html`
              <span>
                <a
                  class="link dark-blue underline-hover"
                  href="https://www.npmjs.com/package/${x}"
                  >${x}</a
                >${i + 1 !== deps.length ? ', ' : null}
              </span>
            `
          })}
        </p>
      </section>
    </body>
  `
}
